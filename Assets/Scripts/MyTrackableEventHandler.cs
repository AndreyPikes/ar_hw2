﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTrackableEventHandler : DefaultTrackableEventHandler
{
    [SerializeField] private AudioSource findSound;
    [SerializeField] private AudioSource lostSound;
    [SerializeField] private GameObject searchMessage;
    private bool isFound = false;

    
    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();        
        searchMessage.SetActive(false);
        findSound.Play();
        isFound = true;
        
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();        
        searchMessage.SetActive(true);
        if (isFound)
        {
            lostSound.Play();
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        base.OnTrackingLost();
        if (searchMessage != null) searchMessage.SetActive(true);
    }
}
