﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DoorBehaviour : MonoBehaviour, IPointerClickHandler
{
    private new Animation animation;

    private bool isNotAnimatedYet = true;

    void Start()
    {
        animation = GetComponent<Animation>();
    }

    public void DoorOpen()
    {
        if (isNotAnimatedYet)
        {
            StartCoroutine(DelayedDoorOpen(0.1f));
            isNotAnimatedYet = false;
        }
    }

    IEnumerator DelayedDoorOpen(float delay)
    {        
        yield return new WaitForSeconds(delay);
        animation.Play();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("нажато");
        DoorOpen();
    }
}
